describe('Test overall view', () => {
  it('Checks overall page looks right', () => {
    cy.visit('http://localhost:4200').then(() => {
      cy.get('body').toMatchImageSnapshot({
        imageConfig: {
          createDiffImage: true,
          threshold: 0.001,
          thresholdType: 'percent',
        },
        name: 'page-test',
      })
    })
  })
})

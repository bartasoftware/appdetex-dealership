import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { CarListingsPage } from './pages/car-listings-page/car-listings.page'
import { CarListingComponent } from './pages/car-listings-page/components/car-listing/car-listing.component'
import { ToolbarComponent } from './components/toolbar/toolbar.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatButtonModule } from '@angular/material/button'
import { MatCardModule } from '@angular/material/card'
import { MatSelectModule } from '@angular/material/select'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { CarFiltersComponent } from './pages/car-listings-page/components/car-filters/car-filters.component'
import { SliderModule } from 'primeng/slider'
import { MatInputModule } from '@angular/material/input'
import { CarSortingComponent } from './pages/car-listings-page/components/car-sorting/car-sorting.component'
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search'
import { MatIconModule } from '@angular/material/icon'
import { MatTooltipModule } from '@angular/material/tooltip'
import { ScrollingModule } from '@angular/cdk/scrolling'
import { MatPaginatorModule } from '@angular/material/paginator'

@NgModule({
  declarations: [
    AppComponent,
    CarListingsPage,
    CarListingComponent,
    ToolbarComponent,
    CarFiltersComponent,
    CarSortingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ScrollingModule,

    //Material Modules
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    MatTooltipModule,
    MatPaginatorModule,

    //extra modules
    SliderModule,
    NgxMatSelectSearchModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

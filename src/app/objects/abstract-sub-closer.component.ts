import { Component, OnDestroy } from '@angular/core'
import { Subject } from 'rxjs'

@Component({
  template: '',
})
export abstract class AbstractSubCloserComponent implements OnDestroy {
  destroySubject: Subject<void> = new Subject<void>()
  constructor() {}

  ngOnDestroy() {
    this.destroySubject.next()
  }
}

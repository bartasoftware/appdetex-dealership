export interface CarModel {
  description: string
  color: string
  make: string
  highwayMileage: { high: number; low: number }
  cityMileage: { high: number; low: number }
  miles: number
  year: number
  model: string
  _id: string
  price: string
}

import { Injectable } from '@angular/core'
// @ts-ignore
import CarData from '../../assets/vehicles.json'
import { CarModel } from '../objects/CarModel'

@Injectable({
  providedIn: 'root',
})
export class CarDataService {
  constructor() {}

  getCarData(): CarModel[] {
    return CarData
  }
}

import { Component, OnInit, ViewChild } from '@angular/core'
import { CarDataService } from '../../services/car-data.service'
import { CarModel } from '../../objects/CarModel'
import { CarLoadingService } from './car-loading.service'
import { PageEvent } from '@angular/material/paginator'

@Component({
  selector: 'app-car-listings-page',
  templateUrl: './car-listings.page.html',
  styleUrls: ['./car-listings.page.scss'],
})
export class CarListingsPage implements OnInit {
  @ViewChild('paginator')
  paginator?
  pagedCars: CarModel[] = []
  filteredCars: CarModel[] = []
  carData: CarModel[] = []
  private _previousPageEvent: PageEvent = { pageSize: 50, pageIndex: 0 } as PageEvent

  constructor(private carDataService: CarDataService, public carLoadingService: CarLoadingService) {}

  ngOnInit(): void {
    this.carData = this.carDataService.getCarData()
    this.pageEvent()
  }

  resetPage() {
    this._previousPageEvent.pageIndex = 0
    this.paginator?.firstPage()
    this.pageEvent()
  }

  pageEvent(event?: PageEvent) {
    this._previousPageEvent = event == null ? this._previousPageEvent : event
    const start = this._previousPageEvent.pageIndex * this._previousPageEvent.pageSize + 1
    const end = (this._previousPageEvent.pageIndex + 1) * this._previousPageEvent.pageSize
    this.pagedCars = this.filteredCars.slice(start - 1, end)
  }
}

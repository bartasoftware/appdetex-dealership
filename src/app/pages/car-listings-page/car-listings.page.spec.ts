import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CarListingsPage } from './car-listings.page'
import { CarDataService } from '../../services/car-data.service'
import { CarLoadingService } from './car-loading.service'

describe('CarListingsPageComponent', () => {
  let component: CarListingsPage
  let fixture: ComponentFixture<CarListingsPage>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CarListingsPage],
      providers: [CarDataService, CarLoadingService],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CarListingsPage)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

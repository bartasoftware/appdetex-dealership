import { Component, Input, OnInit } from '@angular/core'
import { CarModel } from '../../../../objects/CarModel'

@Component({
  selector: 'app-car-listing',
  templateUrl: './car-listing.component.html',
  styleUrls: ['./car-listing.component.scss'],
})
export class CarListingComponent implements OnInit {
  @Input()
  car: CarModel

  constructor() {}

  ngOnInit(): void {}
}

import { CarModel } from '../../../../objects/CarModel'

export enum SortBy {
  MAKE = 'Make',
  MODEL = 'Model',
  PRICE = 'Price',
  ODOMETER = 'Odometer',
  YEAR = 'Year',
}

function carSort<T>(str1: T, str2: T, modifier: number): number {
  if (str1 < str2) return -1 * modifier
  if (str1 > str2) return modifier
  return 0
}

export function sortCars(carInput: CarModel[], sortOrderModifier: number, sortBy: string): CarModel[] {
  const cars = [...carInput]
  let sortedCars: CarModel[]
  switch (sortBy) {
    case SortBy.MAKE:
      sortedCars = cars.sort((car1, car2) => carSort<string>(car1.make, car2.make, sortOrderModifier * -1)) //string modifier is backwards from numbers
      break
    case SortBy.MODEL:
      sortedCars = cars.sort((car1, car2) => carSort<string>(car1.model, car2.model, sortOrderModifier * -1)) //string modifier is backwards from numbers
      break
    case SortBy.PRICE:
      sortedCars = cars.sort((car1, car2) =>
        carSort<number>(
          parseInt(car1.price.replace(/[\W_]+/g, '')),
          parseInt(car2.price.replace(/[\W_]+/g, '')),
          sortOrderModifier
        )
      )
      break
    case SortBy.ODOMETER:
      sortedCars = cars.sort((car1, car2) => carSort<number>(car1.miles, car2.miles, sortOrderModifier))
      break
    case SortBy.YEAR:
      sortedCars = cars.sort((car1, car2) => carSort<number>(car1.year, car2.year, sortOrderModifier))
      break
  }

  return sortedCars
}

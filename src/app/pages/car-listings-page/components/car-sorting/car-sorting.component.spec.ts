import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CarSortingComponent } from './car-sorting.component'
import { CarModel } from '../../../../objects/CarModel'
import { SortBy, sortCars } from './sorting.util'

describe('CarSortingComponent', () => {
  let component: CarSortingComponent
  let fixture: ComponentFixture<CarSortingComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CarSortingComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CarSortingComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should sort correctly', () => {
    const testCars: CarModel[] = [
      {
        make: 'A',
        model: 'A',
        price: '1',
        miles: 1,
        year: 1,
      } as CarModel,
      {
        make: 'B',
        model: 'B',
        price: '2',
        miles: 2,
        year: 2,
      } as CarModel,
    ]
    const testCarsReverse: CarModel[] = [testCars[1], testCars[0]]

    // Make
    let sortResult = sortCars(testCars, -1, SortBy.MAKE)
    expect(sortResult).toEqual(testCars)
    sortResult = sortCars(testCars, 1, SortBy.MAKE)
    expect(sortResult).toEqual(testCarsReverse)

    //Model
    sortResult = sortCars(testCars, -1, SortBy.MODEL)
    expect(sortResult).toEqual(testCars)
    sortResult = sortCars(testCars, 1, SortBy.MODEL)
    expect(sortResult).toEqual(testCarsReverse)

    //Price
    sortResult = sortCars(testCars, 1, SortBy.PRICE)
    expect(sortResult).toEqual(testCars)
    sortResult = sortCars(testCars, -1, SortBy.PRICE)
    expect(sortResult).toEqual(testCarsReverse)

    //Odometer
    sortResult = sortCars(testCars, 1, SortBy.ODOMETER)
    expect(sortResult).toEqual(testCars)
    sortResult = sortCars(testCars, -1, SortBy.ODOMETER)
    expect(sortResult).toEqual(testCarsReverse)

    //Year
    sortResult = sortCars(testCars, 1, SortBy.YEAR)
    expect(sortResult).toEqual(testCars)
    sortResult = sortCars(testCars, -1, SortBy.YEAR)
    expect(sortResult).toEqual(testCarsReverse)
  })
})

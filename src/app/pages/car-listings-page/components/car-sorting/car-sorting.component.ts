import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { CarModel } from '../../../../objects/CarModel'
import { FormControl } from '@angular/forms'
import { CarLoadingService } from '../../car-loading.service'
import { AbstractSubCloserComponent } from '../../../../objects/abstract-sub-closer.component'
import { takeUntil } from 'rxjs/operators'
import { SortBy, sortCars } from './sorting.util'

@Component({
  selector: 'app-car-sorting',
  templateUrl: './car-sorting.component.html',
  styleUrls: ['./car-sorting.component.scss'],
})
export class CarSortingComponent extends AbstractSubCloserComponent implements OnInit {
  _cars: CarModel[] = []
  @Input()
  set cars(cars: CarModel[]) {
    this._cars = cars
    this.sortCars()
  }

  @Output()
  sortedCars: EventEmitter<CarModel[]> = new EventEmitter<CarModel[]>()

  sortByOptions: string[] = Object.values(SortBy)
  sortByControl: FormControl = new FormControl(SortBy.MAKE)

  sortOrderIcon: 'pi-sort-amount-down' | 'pi-sort-amount-up' = 'pi-sort-amount-down'

  constructor(private loadingService: CarLoadingService) {
    super()
  }

  ngOnInit(): void {
    this.sortByControl.valueChanges.pipe(takeUntil(this.destroySubject)).subscribe(() => this.sortCars())
  }

  sortCars() {
    this.loadingService.loading(true)

    const ascDescModifier = this.sortOrderIcon === 'pi-sort-amount-down' ? -1 : 1
    this.sortedCars.next(sortCars(this._cars, ascDescModifier, this.sortByControl.value))

    this.loadingService.loading(false)
  }
}

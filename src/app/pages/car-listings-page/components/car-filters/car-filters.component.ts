import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { CarModel } from '../../../../objects/CarModel'
import { FormControl, FormGroup } from '@angular/forms'
import { debounceTime, takeUntil } from 'rxjs/operators'
import {
  filterCars,
  updateFilterOptions,
  CarFilterControls,
  CarFilterOptions,
  CarFiltersFormValue,
  MIN_YEAR,
  MAX_YEAR,
} from './filters.util'
import { CarLoadingService } from '../../car-loading.service'
import { AbstractSubCloserComponent } from '../../../../objects/abstract-sub-closer.component'

@Component({
  selector: 'app-car-filters',
  templateUrl: './car-filters.component.html',
  styleUrls: ['./car-filters.component.scss'],
})
export class CarFiltersComponent extends AbstractSubCloserComponent implements OnInit {
  _cars: CarModel[] = []
  @Input()
  set cars(cars: CarModel[]) {
    this._cars = cars
    this.filterCars()
    this.filterOptionsSearched = { ...this.filterOptions }
  }

  @Output()
  filteredCars: EventEmitter<CarModel[]> = new EventEmitter<CarModel[]>()

  // These are static values for view
  CarFilterControls = CarFilterControls
  MIN_YEAR = MIN_YEAR
  MAX_YEAR = MAX_YEAR

  // form controls
  filterFormGroup: FormGroup = new FormGroup({
    [CarFilterControls.MAKE]: new FormControl(),
    [CarFilterControls.MODEL]: new FormControl(),
    [CarFilterControls.COLOR]: new FormControl(),
    [CarFilterControls.YEAR]: new FormControl([0, MAX_YEAR]),
  })
  makeOptionFilter: FormControl = new FormControl()
  modelOptionFilter: FormControl = new FormControl()
  colorOptionFilter: FormControl = new FormControl()

  // Source options
  filterOptions: CarFilterOptions = {
    make: [],
    model: [],
    color: [],
  }
  // Filtered down options
  filterOptionsSearched: CarFilterOptions = {
    make: [],
    model: [],
    color: [],
  }

  constructor(private loadingService: CarLoadingService) {
    super()
  }

  ngOnInit(): void {
    this.filterFormGroup.valueChanges
      .pipe(takeUntil(this.destroySubject), debounceTime(300))
      .subscribe(() => this.filterCars())
    this.makeOptionFilter.valueChanges
      .pipe(takeUntil(this.destroySubject), debounceTime(300))
      .subscribe(
        () =>
          (this.filterOptionsSearched.make = this.filterOptions.make.filter((option) =>
            option.toLowerCase().includes(this.makeOptionFilter.value.toLowerCase())
          ))
      )
    this.modelOptionFilter.valueChanges
      .pipe(takeUntil(this.destroySubject), debounceTime(300))
      .subscribe(
        () =>
          (this.filterOptionsSearched.model = this.filterOptions.model.filter((option) =>
            option.toLowerCase().includes(this.modelOptionFilter.value.toLowerCase())
          ))
      )
    this.colorOptionFilter.valueChanges
      .pipe(takeUntil(this.destroySubject), debounceTime(300))
      .subscribe(
        () =>
          (this.filterOptionsSearched.color = this.filterOptions.color.filter((option) =>
            option.toLowerCase().includes(this.colorOptionFilter.value.toLowerCase())
          ))
      )
  }

  clearFilters() {
    const newFilters: CarFiltersFormValue = {
      make: [],
      model: [],
      color: [],
      year: [0, MAX_YEAR],
    }
    this.filterFormGroup.setValue(newFilters)
  }

  // used to match up the number input with range slider
  updateYearRange(newVal, position) {
    if (newVal > 2020 || newVal < 1900) return

    const values = this.filterFormGroup.controls[CarFilterControls.YEAR].value
    values[position] = newVal - MIN_YEAR
    this.filterFormGroup.controls[CarFilterControls.YEAR].setValue(values)
  }

  private filterCars() {
    this.loadingService.loading(true)

    const formGroup = this.filterFormGroup.getRawValue()
    this.filteredCars.next(filterCars(this._cars, formGroup))
    this.filterOptions = updateFilterOptions(this._cars, formGroup)
    this.filterOptionsSearched = { ...this.filterOptions }

    this.loadingService.loading(false)
  }
}

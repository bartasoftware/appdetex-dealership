import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CarFiltersComponent } from './car-filters.component'
import { CarModel } from '../../../../objects/CarModel'
import { CarFilterOptions, CarFiltersFormValue, filterCars, MAX_YEAR, updateFilterOptions } from './filters.util'

describe('CarFiltersComponent', () => {
  let component: CarFiltersComponent
  let fixture: ComponentFixture<CarFiltersComponent>
  let TEST_CARS: CarModel[]

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CarFiltersComponent],
    }).compileComponents()

    TEST_CARS = [
      {
        make: 'A',
        model: 'A',
        color: 'A',
        year: 1900,
      } as CarModel,
      {
        make: 'B',
        model: 'B',
        color: 'B',
        year: 1950,
      } as CarModel,
    ]
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CarFiltersComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should filter correctly', () => {
    const emptyFilters: CarFiltersFormValue = {
      make: [],
      model: [],
      color: [],
      year: [0, MAX_YEAR],
    }

    // make filter
    let filteredCars: CarModel[] = filterCars(TEST_CARS, { ...emptyFilters, make: ['A'] })
    expect(filteredCars.length).toEqual(1)
    expect(filteredCars[0]).toEqual(TEST_CARS[0])

    // model filter
    filteredCars = filterCars(TEST_CARS, { ...emptyFilters, model: ['B'] })
    expect(filteredCars.length).toEqual(1)
    expect(filteredCars[0]).toEqual(TEST_CARS[1])

    // color filter
    filteredCars = filterCars(TEST_CARS, { ...emptyFilters, color: ['A'] })
    expect(filteredCars.length).toEqual(1)
    expect(filteredCars[0]).toEqual(TEST_CARS[0])

    // year filter
    filteredCars = filterCars(TEST_CARS, { ...emptyFilters, year: [0, 10] })
    expect(filteredCars.length).toEqual(1)
    expect(filteredCars[0]).toEqual(TEST_CARS[0])
  })

  it('should filter down options correctly', () => {
    const emptyFormGroup: CarFiltersFormValue = {
      make: [],
      model: [],
      color: [],
      year: [],
    }

    let expectedOptions: CarFilterOptions = {
      make: ['A', 'B'],
      model: [],
      color: ['A', 'B'],
    }

    let filterOptions: CarFilterOptions = updateFilterOptions(TEST_CARS, emptyFormGroup)
    expect(filterOptions).toEqual(expectedOptions)

    expectedOptions = {
      make: ['A', 'B'],
      model: ['A'],
      color: ['A'],
    }
    filterOptions = updateFilterOptions(TEST_CARS, { ...emptyFormGroup, make: ['A'] })
    expect(filterOptions).toEqual(expectedOptions)

    expectedOptions = {
      make: ['A', 'B'],
      model: ['A', 'B'],
      color: ['A', 'B'],
    }
    filterOptions = updateFilterOptions(TEST_CARS, { ...emptyFormGroup, make: ['A', 'B'] })
    expect(filterOptions).toEqual(expectedOptions)

    expectedOptions = {
      make: ['A'],
      model: [],
      color: ['A', 'B'],
    }
    filterOptions = updateFilterOptions(TEST_CARS, { ...emptyFormGroup, color: ['A'] })
    expect(filterOptions).toEqual(expectedOptions)

    expectedOptions = {
      make: ['A'],
      model: ['A'],
      color: ['A'],
    }
    filterOptions = updateFilterOptions(TEST_CARS, { ...emptyFormGroup, make: ['A'], color: ['A'] })
    expect(filterOptions).toEqual(expectedOptions)

    expectedOptions = {
      make: ['A', 'B'],
      model: ['A'],
      color: ['A'],
    }
    filterOptions = updateFilterOptions(TEST_CARS, { ...emptyFormGroup, make: ['A'], model: ['A'] })
    expect(filterOptions).toEqual(expectedOptions)

    expectedOptions = {
      make: ['A'],
      model: ['A'],
      color: ['A'],
    }
    filterOptions = updateFilterOptions(TEST_CARS, { ...emptyFormGroup, make: ['A'], model: ['A'], color: ['A'] })
    expect(filterOptions).toEqual(expectedOptions)
  })
})

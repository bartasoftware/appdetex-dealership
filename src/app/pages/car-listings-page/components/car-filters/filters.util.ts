import { CarModel } from '../../../../objects/CarModel'
import { uniq } from 'lodash'

export enum CarFilterControls {
  MAKE = 'make',
  MODEL = 'model',
  COLOR = 'color',
  YEAR = 'year',
}

export interface CarFilterOptions {
  make: string[]
  model: string[]
  color: string[]
}

export interface CarFiltersFormValue {
  make: string[]
  model: string[]
  color: string[]
  year: number[]
}

export const MIN_YEAR = 1900
export const MAX_YEAR = new Date().getFullYear() - MIN_YEAR

function stringFilter(val: string, filters: string[]): boolean {
  return filters.length === 0 || filters.find((filter) => filter === val) != null
}

export function makeFilter(car: CarModel, formGroup: CarFiltersFormValue): boolean {
  return !formGroup.make || stringFilter(car.make, formGroup.make)
}

export function modelFilter(car: CarModel, formGroup: CarFiltersFormValue): boolean {
  return !formGroup.model || stringFilter(car.model, formGroup.model)
}

export function colorFilter(car: CarModel, formGroup: CarFiltersFormValue): boolean {
  return !formGroup.color || stringFilter(car.color, formGroup.color)
}

export function filterCars(cars: CarModel[], formGroup: CarFiltersFormValue): CarModel[] {
  return cars
    .filter((car) => makeFilter(car, formGroup))
    .filter((car) => modelFilter(car, formGroup))
    .filter((car) => colorFilter(car, formGroup))
    .filter((car) => car.year - MIN_YEAR >= formGroup.year[0] && car.year - MIN_YEAR <= formGroup.year[1])
}

export function updateFilterOptions(cars: CarModel[], formGroup: CarFiltersFormValue): CarFilterOptions {
  return {
    make: uniq(cars.filter((car) => colorFilter(car, formGroup)).map((car) => car.make)),
    model:
      formGroup.make && formGroup.make.length > 0
        ? uniq(
            cars
              .filter((car) => makeFilter(car, formGroup))
              .filter((car) => colorFilter(car, formGroup))
              .map((car) => car.model)
          )
        : [],
    color: uniq(
      cars
        .filter((car) => makeFilter(car, formGroup))
        .filter((car) => modelFilter(car, formGroup))
        .map((car) => car.color)
    ),
  }
}

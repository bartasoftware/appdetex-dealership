import { TestBed } from '@angular/core/testing'

import { CarLoadingService } from './car-loading.service'

describe('CarLoadingService', () => {
  let service: CarLoadingService

  beforeEach(() => {
    TestBed.configureTestingModule({})
    service = TestBed.inject(CarLoadingService)
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })
})

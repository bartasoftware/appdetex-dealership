import { Injectable } from '@angular/core'
import { Observable, Subject } from 'rxjs'

@Injectable({
  providedIn: 'root',
})
export class CarLoadingService {
  private loadingSubject: Subject<boolean> = new Subject()
  loading$: Observable<boolean> = this.loadingSubject.asObservable()

  constructor() {}

  loading(isLoading: boolean) {
    this.loadingSubject.next(isLoading)
  }
}

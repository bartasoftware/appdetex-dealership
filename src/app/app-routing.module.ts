import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { CarListingsPage } from './pages/car-listings-page/car-listings.page'

const routes: Routes = [
  { path: '', component: CarListingsPage },
  { path: 'listings', component: CarListingsPage },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
